// EXO 1 :
(function() {
	var name     = "LEYGUES",
		forename = "Flavien",
		result;

	result = name + " " + forename;

	alert(result); // Affiche "LEYGUES Flavien"
});

// EXO 2 :
(function() {
	var name, forename, age, result;

	name     = prompt("Votre nom ?");
	forename = prompt("Votre prénom ?");
	age      = prompt("Votre âge ?");

	result = "Bonjour et bienvenue " + name + " " + forename + " vous avez : " + age + " ans";
	alert(result);
});

// EXO 3 :
(function() {
	var age = parseInt( prompt('Quel est votre âge ?') );

	if (age > 0) {
		alert("Bienvenue");
	}
	else {
		alert("Merci de renseigner un âge valide");
	}
});

// EXO 4-1 :
(function() {
	var age, onContinue = true;

	do {

		age = prompt('Quel est ton âge ?');
		
		if (age >= 18) {
			onContinue = false;
		}

	} while(onContinue == true);

	alert("Bienvenue");
});

// EXO 4-2 :
(function() {

	var note,
		somme = 0,
		moyenne,
		noteMax = 0,
		nbNotes = 0,
		onContinue = true;

	do {

		note = parseInt( prompt("Rentrez une note comprise entre 0 et 20 :") );

		// On vérifie si la note est bien comprise entre 0 et 20
		if (note >= 0 && note <= 20){

			nbNotes++; // On incrémente le nombre de notes
			somme += note; // On ajoute à la variable somme la note rentrée

			// On vérifie si la note rentrée est supérieure à la note maximale connue
			if (note > noteMax) {
				noteMax = note; // Si oui, on met à jour la note maximale
			}
		}
		// Sinon, on sort de la boucle
		else {
			onContinue = false;
		}

	} while(onContinue == true);

	moyenne = somme / nbNotes;

	console.log('Le nombre de note rentré est de : ' + nbNotes);
	console.log('La moyenne est de : ' + moyenne);
	console.log('La note maximale est de : ' + noteMax);
});

(function() {

	var note,
		nbNotes = parseInt( prompt('Rentrez le nombre de notes à ajouter :') ),
		somme = 0,
		moyenne,
		noteMax = 0,
		onContinue,
		compteur = 0;

	// On boucle en fonction du nombre de notes à enregistrer
	for (var i = 0; i < nbNotes; i++) {
		onContinue = true; // On initialise la variable d'arrêt de la boucle do/while avant de commencer

		do {
			note = parseInt( prompt("Rentrez la note N°" + (i+1) )); //On demande à lutilisateur de rentrer la note

			// SI la note rentrée >= 0 ET <= 20 {
			if (note >= 0 && note <= 20) {
				somme += note; //on ajoute la note à somme des notes

				// SI la note rentrée > note max : la note rentrée devient la nouvelle note maximale
				if (note > noteMax) {
					noteMax = note;
				}
				onContinue = false; // On change la valeur de la variable afin de sortir de la boucle do/while
				compteur++; // On incrémente un compteur afin de calculer le nombre de notes rentrées par l'utilisateur
			}
			// SINON on demande à l'utilisateur si il souhaite arrêter ou continuer
			else {
				if (confirm('Voulez vous arrêter ?') == true) {
					i = nbNotes; // On change la variable i afin d'arrêter la boucle for
					break; // On utilise break afin d'arrêter la boucle do/while
				}
			}
		}
		while(onContinue == true);
	}

	moyenne = somme / compteur;

	console.log('Le nombre de note rentré est de : ' + compteur);
	console.log('La moyenne est de : ' + moyenne);
	console.log('La note maximale est de : ' + noteMax);
})();