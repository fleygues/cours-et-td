(function(){

	// ---------------
	// ---- EXO 1 ----
	// ---------------

	// On déclare une variable de type tableau et on assigne des valeurs
	var arrayNotes = [5,7,12,8,16];

	console.log(arrayNotes[0]); // Affiche le premier item (indice 0)
	console.log(arrayNotes[4]); // Affiche le dernier item (indice 4)

	// 1.3
	// On demande à l'utilisateur de rentrer 3 notes :
	var note;
	for (var i = 0; i < 3; i++) {
		note = parseInt(prompt('Rentrez une note entre 0 et 20'));
		arrayNotes.push(note);
	}

	// 1.4
	// On affiche chaque note dans la console
	console.log('Exercice 1.4');
	for(var i = 0; i < arrayNotes.length; i++){
		console.log(arrayNotes[i]);
	}

	// 1.5
	// Déclaration d'une fonction qui calcule la somme d'un tableau
	var somme = 0; // On déclare la variable en dehors de la fonction afin de pouvoir l'utiliser dans les autres fonctions (average)
	function totalNotes() {

		for (var i = 0; i < arrayNotes.length; i++) {
			somme = somme + arrayNotes[i];
		}
		console.log("La somme est de : " + somme);
	}
	totalNotes(); // Ne pas oublier d'appeler la fonction pour l'utiliser

	// 1.6
	// Déclaration d'une fonction qui compare les notes et indique la note maximale d'un tableau
	function noteMax() {
		var max = arrayNotes[0];

		for (var i = 0; i < arrayNotes.length; i++) {
			if (arrayNotes[i] > max) {
				max = arrayNotes[i];
			}
		}

		console.log("La note maximale est de : " + max);
	}
	noteMax(); // Ne pas oublier d'appeler la fonction pour l'utiliser

	// 1.7
	// Déclaration d'une fonction qui compare les notes et indique la note minimale d'un tableau
	function noteMin() {
		var min = arrayNotes[0];

		for (var i = 0; i < arrayNotes.length; i++) {
			if (arrayNotes[i] < min) {
				min = arrayNotes[i];
			}
		}

		console.log("La note minimale est de : " + min);
	}
	noteMin(); // Ne pas oublier d'appeler la fonction pour l'utiliser

	// 1.8
	// Déclaration d'une fonction qui calcule la moyenne.
	function average() {
		var moyenne;
		
		moyenne = somme / arrayNotes.length;
		console.log("La moyenne est de : " + moyenne);
	}
	average(); // Ne pas oublier d'appeler la fonction pour l'utiliser
});

(function(){

	// ---------------
	// ---- EXO 2 ----
	// ---------------

	// 2.1

	// Déclaration des variables
	var note,
		arrayNotes = [], // Déclaration d'une variable de type tableau
		proceed    = true;

	do {
		// On demande à l'utilisateur de renseigner une note comprise entre 0 et 20
		note = parseInt(prompt("Rentrez une note (comprise entre 0 et 20) :"));

		// SI, la note rentrée par l'utilisateur est bien comprise entre 0 et 20, la fonction checkNote() retourne "true", sinon elle retourne "false", cf la définition de la fonction plus bas dans le code
		if ( checkNote(note) == true ) {
			// EXO 2.2
			// On ajoute la note de l'utilisateur au tableau de notes à l'aide de la méthode .push() (nous aurions également pu utiliser la méthode .unshift())
			arrayNotes.push(note);
		}
		else {
			// On modifie la variable afin d'arrêter la boucle
			proceed = false;
		}
	} while(proceed == true); // On teste la condition d'arrêt

	// EXO 2.4
	// On affiche les notes du tableau et le nombre de notes (nombre d'éléments dans le tableau) dans la console
	console.log('Il y a ' + arrayNotes.length + ' notes qui sont : ' + arrayNotes);

	// EXO 2.5
	console.log('La note maximale du tableau est de : ' + noteMax(arrayNotes)); // On appelle la fonction noteMax() qui nous retourne la valeur maximale du tableau envoyé en paramètre, cf la définition de la fonction plus bas dans le code
	console.log('La note minimale du tableau est de : ' + noteMin(arrayNotes)); // On appelle la fonction noteMin() qui nous retourne la valeur minimale du tableau envoyé en paramètre, cf la définition de la fonction plus bas dans le code
	console.log('La moyenne du tableau est de : ' + average(arrayNotes)); // On appelle la fonction average() qui nous retourne la moyenne calculée du tableau envoyé en paramètre, cf la définition de la fonction plus bas dans le code
})();

// EXO 2.3
// Déclaration de la fonction "checkNote" avec un argument nommé "note" (nous pouvons le nommer comme nous le souhaitons)
function checkNote(note){
	// SI la note envoyée lors de l'appel de la fonction est entre 0 et 20 :
	if (note >= 0 && note <= 20) {
		return true; // on retourne true
	}
	else {
		return false; // sinon on retourne false
	}
}

function noteMax(tableauNotes) {
	// On définit la variable locale à la fonction "max" et on lui assigne la première valeur du tableau envoyé lors de l'appel de la fonction
	var max = tableauNotes[0];

	// On parcourt le tableau afin de vérifier si la Ième note est supérieur à la valeur actuelle de la note maximale (variable "max")
	for (var i = 0; i < tableauNotes.length; i++) {
		if (tableauNotes[i] > max) {
			max = tableauNotes[i]; // Si la Ième note est supérieur, on stocke la nouvelle valeur
		}
	}

	return max; // On retourne la valeur maximale du tableau
}

function noteMin(tableauNotes) {
	// Fonction identique à noteMax() (sauf changement de la condition afin de retourner la plus petite valeur contenue dans le tableau)
	var min = tableauNotes[0];

	for (var i = 0; i < tableauNotes.length; i++) {
		if (tableauNotes[i] < min) {
			min = tableauNotes[i];
		}
	}

	return min;
}

function average(tableauNotes) {
	// On définit les variables locales à la fonction dont on a besoin
	var moyenne,
		somme = 0;
	
	// On boucle sur l'ensemble des éléments du tableau
	for (var i = 0; i < tableauNotes.length; i++) {
		somme = somme + tableauNotes[i]; // On ajoute la valeur de la Ième note du tableau à la variable somme. Possible de faire : somme += tableauNotes[i];
	}

	moyenne = somme / tableauNotes.length; // On calcule la moyenne
	return moyenne; // On retourne la valeur obtenue
}